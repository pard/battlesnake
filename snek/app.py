from flask import Flask, request
from handler import InterceptHandler
from loguru import logger
from game import Game
import sys


app = Flask(__name__)
game = Game({})


@app.route('/')
def index():
    payload = {
        "apiversion": "1",
        "author": "Ian S. Pringle",
        "color": "#444444",
        "head": "default",
        "tail": "default",
    }
    return payload


@app.route('/start', methods=['POST'])
def start():
    game = Game(request, logger=logger)
    return "ok"


@app.route('/move', methods=['POST'])
def move():
    m = game.move(request)
    logger.info(m)
    return m


@app.route('/end', methods=['POST'])
def end():
    logger.info("end")
    return "ok"


@logger.catch
def main():
    logger.remove()
    logger.add(
        sys.stderr,
        colorize=True,
        backtrace=True,
        diagnose=True,
        format=(
            "<green>{time:YYYY-MM-DD at HH:mm:ss}</green> "
            "<white>|</white> "
            "<level>{level}</level> "
            "<white>|</white> "
            "{message}"
                ),
    )
    handler = InterceptHandler()
    handler.setLevel(0)
    app.logger.addHandler(handler)

    app.run()


if __name__ == "__main__":
    main()
