class Game:
    up = "up"
    down = "down"
    left = "left"
    right = "right"
    moves = [up, down, left, right]

    def __init__(self, request, logger=None):
        if request == {}:
            self.game = {}
            self.turn = 0
            self.board = {}
            self.you = {}
        else:
            data = request.get_json()
            self.game = data.get('game')
            self.turn = data.get('turn')
            self.board = data.get('board')
            self.you = data.get('you')
        self.last_move = None
        self.logger = logger
        self.target_reached = False

    def border(self):
        last = self.last_move

    def circle(self, clockwise=True, initial=None):
        last = self.last_move
        if last == self.up:
            if clockwise:
                n = self.right
            else:
                n = self.left
        elif last == self.right:
            if clockwise:
                n = self.down
            else:
                n = self.up
        elif last == self.down:
            if clockwise:
                n = self.left
            else:
                n = self.right
        elif last == self.left:
            if clockwise:
                n = self.up
            else:
                n = self.down
        else:
            if initial:
                n = initial
            else:
                n = self.up
        self.last_move = n
        return n

    def go_to(self, target=[0, 0]):
        curr = [v for _, v in self.you.get('head').items()]
        self.log(curr)
        if curr == target:
            self.target_reached = True
            return None
        x_diff = curr[0] - target[0]
        self.log(f"x_diff: {x_diff}")
        y_diff = curr[1] - target[1]
        self.log(f"y_diff: {y_diff}")
        self.log(curr)
        if x_diff != 0:
            if x_diff > 0:
                m = self.left
            else:
                m = self.right
        else:
            if y_diff != 0:
                if y_diff > 0:
                    m = self.down
                else:
                    m = self.up
            else:  # this should never happen
                self.target_reached = True
                m = None
        self.log(m)
        return m

    def log(self, message, level="info"):
        if self.logger:
            self.logger.info(message)

    def move(self, request):
        self.update(request)
        m = self.go_to()
        if m and not self.target_reached:
            return self._move(m)
        else:
            return self._move(self.circle(clockwise=False, initial=self.right))

    def solo_survival(self, request):
        self.update(request)
        m = self.go_to()
        if m and not self.target_reached:
            return self._move(m)
        else:
            return self._move(self.circle(clockwise=False, initial=self.right))

    def update(self, request):
        data = request.get_json()
        self.game = data.get('game')
        self.turn = data.get('turn')
        self.board = data.get('board')
        self.you = data.get('you')

    def _move(self, direction):
        return {"move": direction}
